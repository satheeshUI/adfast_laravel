<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use FacebookAds\Object\AdCreative;
use FacebookAds\Object\AdCreativeLinkData;
use FacebookAds\Object\Fields\AdCreativeLinkDataFields;
use FacebookAds\Object\AdCreativeObjectStorySpec;
use FacebookAds\Object\Fields\AdCreativeObjectStorySpecFields;
use FacebookAds\Object\Fields\AdCreativeFields;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Api;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Logger\CurlLogger;

class AdsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    
        $account_id = "3503940843024787";
        $appSecret = "6dbba0723a43397f3fe4ce9062633aea";
        $business_id = '291851761972027';
        $accessToken = "EAAxy0M1DMZAMBAJrpCvf4nOzVjWTuQu4ZCOyLkq2SGy29cHRdQxLoByXtRr8b8rRZC9retfUkwQb0qKWn0ooZCFS3Gb8bSqNx5i6Ae8NaP7WHoLfPsnTZCDnMIO6gCYrrXl2KCtTZA2UDIgZCabPgbQZBA3GaW764erWGkyQfYR77nna1ZCzPvpFSLErspXYMqLAZD";
        $page_id = '108265017758755';
        
        $access_token = 'EAAFrIKn1bS0BADpTXWgq3ewYAdnwCopem7odiEEZCGLnjlF2pK36LmsIQDajUHE6xx7ZAMFmGxRyQhfxGkqtEZB9Wt9G4ciGPVSMY0uNEhbQRC8ao8pcfT8WgcJyYZB3mZBUXw5ltndUdjxbfghnjN589JbWPvt3vwZCwRtspzFnZCFDl57KmLJpn3Gt86itrKQGYcCN94aIAZDZD';
        $app_secret = 'ec1e2953c190b7d7fbc77e8c882a6122';
        $ad_account_id = '271617687616957';
        $business_id = '111971763620922';
        $page_id = '418469001548080';
        $pixel_id = '3819416071423222';
        $app_id = '399263011269933';

        $fields = array(
        );
        $api = Api::init($app_id, $app_secret, $access_token);
        $api->setLogger(new CurlLogger());
        $params = array(
            'name' => 'My campaign',
            'objective' => 'LINK_CLICKS',
            'status' => 'PAUSED',
            'special_ad_categories' => array(),
          );
          echo json_encode((new AdAccount($business_id))->createCampaign(
            $fields,
            $params
          )->exportAllData(), JSON_PRETTY_PRINT);
        
    }
}
