<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ads;
use App\Models\AdsMedia;

class UserLogout extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        auth()->logout();
        return response()->json(['status' => true , 'message'=>'User logged out successfully']); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $test = $request->validate([
            'title'=>'required',
            'sub_title'=>'required',
            'url'=>'required',
            'desc'=>'required',    
            'budge'=>'required',
            'target'=>'required',
            'ads'=>'required',
        ]);
        if($test){
            $create_ads = new Ads;
            $ad_data = $request->except(['ads']);
            $ad_data['image'] = 1;
            $ad_data['created_by'] = auth()->user()->id;
            $ad_data['updated_by'] = auth()->user()->id;
            $created_details =  $create_ads->create($ad_data);
            if($created_details){
                $sm_details = $request->ads;
                foreach($sm_details  as $sm_detail){
                    $create_ads = new AdsMedia;
                    $new_array = array();
                    $new_array['ad_id'] = $created_details->id;
                    $new_array['media_id'] = $sm_detail['media_id'];
                    $create_ads->create($new_array);
                }
            }
            return response()->json(['status' => true , 'message'=>'Ads Inserted Successfully']); 
        }
        else{
            return response()->json(['status' => false , 'message'=>'Fill all mandatory fields']); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
