<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    use HasFactory;
    protected $table = 'ads';
	protected $primaryKey = 'id';
	protected $perPage = 25; 
	protected $fillable = [
        'title',
        'sub_title',
        'url',
        'desc',
        'budge',
        'image',
        'target',
        'created_by',
        'updated_by',
    ];

    public function ImageUpload($file, $add_name=''){
        $imageName =  $add_name.time().$file->getClientOriginalName();
        $imageName = $this->clean($imageName);
        $file->move(base_path().env('APP_ADMIN_IMAGE_UPLOADS'), $imageName);
        return $imageName;
    }

    public function SiteFavicon(){
        if ($this->image) {
            return url(env('APP_ADMIN_IMAGE_PATH').'/'.$this->image);
        } else {
            return url('public/admin/img/default_image.png');
        }
    }

    function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^a-zA-Z0-9_.]/', '-', $string);
    }

    public function social_media(){
        return $this->hasMany(AdsMedia::class,  'ad_id', 'id');
    }

}
