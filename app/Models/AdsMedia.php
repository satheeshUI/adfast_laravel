<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdsMedia extends Model
{
    use HasFactory;
    protected $table = 'ads_media';
	protected $primaryKey = 'id';
	protected $perPage = 25; 
	protected $fillable = [
        'ad_id',
        'media_id'
    ];
}
