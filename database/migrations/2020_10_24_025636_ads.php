<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Ads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',100);
            $table->string('sub_title',200);
            $table->string('url',100);
            $table->text('desc');
            $table->string('image',100)->nullable();
            $table->string('budge',100);
            $table->text('target');
            $table->unsignedInteger('created_by')->length(11)->nullable()->comment('User id as reference');
            $table->unsignedInteger('updated_by')->length(11)->nullable()->comment('User id as reference');
            $table->timestamps();
        });

        Schema::table('ads', function($table) {
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
