<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AdsMedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads_media', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ad_id')->length(11)->nullable()->comment('User id as reference');
            $table->string('media_id')->length(11)->nullable();
            $table->timestamps();
        });

        Schema::table('ads_media', function($table) {
            $table->foreign('ad_id')->references('id')->on('ads')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
