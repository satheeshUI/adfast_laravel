<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SocialMedias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_media', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->text('client_id');
            $table->text('secreat_id');
            $table->integer('status')->length(1)->default(1)->nullable()->comment('0 - inactive | 1 - active');
            $table->unsignedInteger('created_by')->length(11)->nullable()->comment('User id as reference');
            $table->unsignedInteger('updated_by')->length(11)->nullable()->comment('User id as reference');
            $table->timestamps();
        });

        Schema::table('social_media', function($table) {
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
