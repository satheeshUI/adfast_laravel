require('./bootstrap');
import Vue from 'vue'
import App from '@/App.vue'
import router from '@/router'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import { ValidationObserver, ValidationProvider, extend } from 'vee-validate';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '@/assets/scss/main.scss'
import * as rules from 'vee-validate/dist/rules';
import Lang from 'vuejs-localization'
import store from '@/store'
Lang.requireAll(require.context('./lang', true, /\.js$/));
Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);
Object.keys(rules).forEach(rule => {
  extend(rule, rules[rule]);
});
Vue.use(Lang)
const app = new Vue({
  el:'#app',
  router:router,
  store:store,
  render: h => h(App)
})
export default app
