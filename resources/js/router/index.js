import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/pages/Dashboard'
import NewAd from '@/pages/NewAd'
import Thanks from '@/pages/Thanks'
import Profile from '@/pages/Profile'
import AllMyAds from '@/pages/AllmyAds'
Vue.use(Router)
export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: "Dashboard",
            component: Dashboard
        },
        {
            path: '/home',
            name: "Dashboard",
            component: Dashboard
        },
        {
            path: '/dashboard',
            name: "Dashboard",
            component: Dashboard
        },
        {
            path: "/newadd",
            name: "NewAd",
            component: NewAd
        },
        {
            path: "/thanks",
            name: "Thanks",
            component: Thanks
        },
        {
            path: "/profile",
            name: "Profile",
            component: Profile
        },
        {
            path: "/allmyads",
            name: "AllMyAds",
            component: AllMyAds
        }
    ]
})