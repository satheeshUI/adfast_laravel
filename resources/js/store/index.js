import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions'
import mutations from './mutations'

Vue.use(Vuex)

const store = new Vuex.Store({
    strict: false,
    state: {
        activePageTitle: 'Home',
        activePageActions: [
            {
                icon: 'power',
                action: 'logout',
                actionType: 'click'
            },
            {
                icon: 'house-door-fill',
                action: '/dashboard',
                actionType: 'link'
            }
        ],
        dashboardActions: [
            {
                icon: 'plus',
                text: 'create new add',
                action: 'newadd',
                actionType: 'link'
            },
            {
                icon: 'plus',
                text: 'my profile',
                action: 'profile',
                actionType: 'link'
            },
            {
                icon: 'plus',
                text: 'all my ads',
                action: 'allmyads',
                actionType: 'link'
            },
            {
                icon: 'plus',
                text: 'log out',
                action: 'logout',
                actionType: 'click'
            }
        ],
        newAd: {
            activeState: 0,
            formData: [
                {
                    step: [
                        {
                            label: 'Headline',
                            inputType: 'text',
                            name : 'title',
                            value: '',
                            rules: "required",
                            placeholder: 'Enter Text'
                        },
                        {
                            label: 'subtitle',
                            inputType: 'text',
                            name : 'sub_title',
                            value: '',
                            rules: "required",
                            placeholder: 'Enter Text'
                        },
                        {
                            label: 'link',
                            inputType: 'text',
                            rules: "required",
                            name : 'url',
                            value: '',
                            placeholder: 'Enter Text'
                        },
                        {
                            label: 'text',
                            inputType: 'textarea',
                            name : 'desc',
                            value: '',
                            rules: "required",
                            placeholder: 'Enter Text'
                        }
                    ]   
                },
                {
                    step: [
                        {
                            label: 'image/video',
                            inputType: 'imageupload',
                            name : 'image',
                            value: ''
                        },
                        {
                            label: 'daily budget',
                            inputType: 'text',
                            name : 'budge',
                            rules: "required",
                            value: '',
                            placeholder: 'the currency sign'
                        },
                        {
                            label: 'in your own words, who is your target audience',
                            inputType: 'textarea',
                            name : 'target',
                            rules: "required",
                            value: '',
                            placeholder: 'for example: students in Mumbai'
                        }
                    ]
                }
            ],
            destinations: {
                title: 'Where to post?',
                list: [
                    {
                        text: 'facebook',
                        isSelected: false
                    },
                    {
                        text: 'google',
                        isSelected: false
                    },
                    {
                        text: 'instagram',
                        isSelected: false
                    }
                ]
            }
        },
        thanksData: {
            navList: [
                {
                    text: 'create new ad',
                    icon: '',
                    link: '/newadd'
                },
                {
                    text: 'all my ads',
                    icon: '',
                    link: '/allmyads'
                },
                {
                    text: 'My Profile',
                    icon: '',
                    link: 'profile'
                },
                {
                    text: 'home',
                    icon: '',
                    link: '/dashboard'
                }
            ]
        }
    },
    mutations,
    actions
})

export default store