@extends('layouts.auth')
@section('content')
<form method="POST" action="{{ route('login') }}" class="text-left" autocomplete="nope">
    @csrf
    <div class="form-group row" >
        <label for="email" class="col-md-12 col-form-label">{{ __('E-Mail Address') }}</label>
        <div class="col-md-12">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required  autofocus placeholder="Enter E-Mail Address" autocomplete="off">
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="password" class="col-md-12 col-form-label">{{ __('Password') }}</label>
        <div class="col-md-12">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required  placeholder="Enter Password" autocomplete="off">
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <button type="submit" class="btn capital fontl authForm__form__btn btn-secondary btn-block">Login</button>
    <a href="/login?state=login" aria-current="page" class="capital custom-link text-center d-block authlink router-link-exact-active router-link-active">forgot password?</a>
    <a href="{{url('register')}}" class="capital custom-link text-center d-block authlink router-link-active">signup</a>
</form>
@endsection
