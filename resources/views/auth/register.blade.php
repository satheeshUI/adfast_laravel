@extends('layouts.auth')
@section('content')
<form method="POST" action="{{ route('register') }}">
    @csrf
    
    <div class="form-group row">
        <label for="email" class="col-md-12 col-form-label">{{ __('E-Mail Address') }}</label>
        <div class="col-md-12">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required  placeholder="Enter email">
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="password" class="col-md-2 col-form-label">{{ __('Password') }}</label>
        <div class="col-md-12">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required  placeholder="Enter password">
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="name" class="col-md-12 col-form-label">{{ __('Full Name') }}</label>
        <div class="col-md-12">
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required  autofocus placeholder="Enter full name ">
            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
   
    <button type="submit" class="btn capital fontl authForm__form__btn btn-secondary btn-block">Signup</button>
    <a href="{{url('login')}}" class="capital custom-link text-center d-block authlink router-link-active">already have an account?</a>
</form>
@endsection
