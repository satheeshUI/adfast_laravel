<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/front.css') }}" rel="stylesheet">
</head>
<body>
   
        <div class="loginWrapper">
            <h4 class="loginWrapper__title">adfast</h4>
            <div class="authForm">
                <h6 class="authForm__title fontm">sign up</h6>
                <div class="socialmedia__btns">
                    <button type="button" class="btn socialmedia__btns__btn btn-secondary google"><a href="{{ route('social.oauth', 'google') }}"> <img src="{{ asset('img/google.png') }}" alt="google"/> continue with google</a></button>
                    <button type="button" class="btn socialmedia__btns__btn btn-secondary facebook"><a href="{{ route('social.oauth', 'facebook') }}"><img src="{{ asset('img/facebook.png') }}" alt="facebook"/>  continue with facebook</a></button>
                    <button type="button" class="btn socialmedia__btns__btn btn-secondary instagram"><a href="{{url('instagram')}}">continue with instagram <img src="{{ asset('img/instagram.png') }}" alt="instagram"/></a> </button>
                </div>
                <h6 class="authForm__or">or</h6>
                <div class="authForm__form">
                        @yield('content')
                </div>
            </div>
        </div>
   
</body>
</html>
