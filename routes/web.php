<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('auth/social', 'App\Http\Controllers\Auth\LoginController@show')->name('social.login');
Route::get('oauth/{driver}', 'App\Http\Controllers\Auth\LoginController@redirectToProvider')->name('social.oauth');
Route::get('oauth/{driver}/callback', 'App\Http\Controllers\Auth\LoginController@handleProviderCallback')->name('social.callback');
Route::get('storage/{filename}', function ($filename){
    $path = base_path('public/ads/img/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});
Auth::routes();
Route::group(['middleware' => ['instagram']], function(){
    Route::get('/search', 'App\Http\Controllers\AppController@search');
    Route::get('/instagram', 'App\Http\Controllers\InstagramController@redirectToInstagramProvider');
    Route::get('/instagram/callback', 'App\Http\Controllers\InstagramController@handleProviderInstagramCallback');
});
Route::resource('/user_logout', 'App\Http\Controllers\User\UserLogout');
Route::resource('/user_add_creation', 'App\Http\Controllers\User\CreateNewAds');
Route::resource('/user_all_ads', 'App\Http\Controllers\User\AllAds');
Route::resource('/user_ads', 'App\Http\Controllers\AdsController');
Route::resource('/user_googel_ads', 'App\Http\Controllers\GoogleAdsController');

Route::get('/{any}', 'App\Http\Controllers\FrontPageController@index')->where('any', '.*');

